const { accessKeyId, secretAccessKey, region } = require('../config/db');
const aws = require('aws-sdk');
const fs = require('fs');
const path = require('path')
const os = require("os");

const express = require('express');
const routes = express.Router();

const s3 = new aws.S3({
  region: region,
  accessKeyId: accessKeyId,
  secretAccessKey: secretAccessKey
});

// sldAsset/
routes.post('/', (req, res) => {
    const filePath = path.join(os.userInfo().homedir, 'Downloads', `${req.body.fileName}`)
    const content = JSON.stringify(req.body.content, null, 4)
    fs.writeFile(filePath, content, err => {
      if (err) {
        console.error(err)
        return
      }
      //file written successfully
      fs.readFile(filePath, (err, buff) => {
        if(err) {
          console.log('ERROR', err);
          res.status(400).json({message: err})
        } else {
          const objectType = 'application/json'
          const params = {
            Bucket:  'scada4x-assets',
            Key: `sldAsset/${req.body.fileName}`,
            Body:  buff,
            ACL: 'public-read',
            ContentType: objectType
          }
  
          s3.upload(params, function(s3Err, result) {
            if(s3Err) {
              throw s3Err 
            } else {
              // fs.unlink(path.join(__dirname, '..', 'temp', `${req.file.originalname}`), (err) => {
              //   if(err) {
              //     throw (err);
              //   }
              // })
              res.status(200).json({ message: 'success' })
            }
          })
        }
      })
    })
})

module.exports = routes;

