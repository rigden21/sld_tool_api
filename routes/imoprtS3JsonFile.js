const aws = require('aws-sdk');
const express = require('express');
const routes = express.Router();
const { accessKeyId, secretAccessKey, region } = require('../config/db');

const s3 = new aws.S3({
  region: region,
  accessKeyId: accessKeyId,
  secretAccessKey: secretAccessKey
});

routes.get('/:name', (req, res) => {
  const params = {
    Bucket: 'scada4x-assets',
    Key: `sldAsset/${req.params.name}`
  }

  s3.getObject(params, function (err, data) {
    if (err) {
      res.status(400).json(err.message)
    } else {
      const content = JSON.parse(data.Body.toString())
      res.status(200).json(content)
    }
  })
})

module.exports = routes;

