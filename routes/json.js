const { accessKeyId, secretAccessKey, region } = require('../config/db');
const aws = require('aws-sdk')
const express = require('express')
const router = express.Router()

const s3 = new aws.S3({
  region: region,
  accessKeyId: accessKeyId,
  secretAccessKey: secretAccessKey
});

router.get('/', (req, res) => {
  const params = {
    Bucket: 'scada4x-assets',
    Prefix: 'sldAsset/',
    Delimiter: '/'
  }

  s3.listObjectsV2(params, function(err, data) {
    if (err) console.log(err, err.stack); 
    else  res.status(200).json(data)
  });
})

module.exports = router