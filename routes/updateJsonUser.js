const {
  QueryTypes
} = require('sequelize');
const model = require('../db/model');

const express = require('express')
const router = express.Router()

async function getUserData(userId) {
  const sQuery = `
  SELECT 
    user_id,
    sld_update
  FROM
    tbl_user_data
  WHERE sld_update IS NOT NULL;`;

  return await model.query(sQuery, {
    type: QueryTypes.SELECT,
  }).then(res => {
    return res
  }).catch(err => console.log(err, 'ERROR'))
}

async function updateSldUpdate(userId, fileName) {
  const sQuery = `
  UPDATE tbl_user_data
SET sld_update = '${fileName}'
WHERE user_id = ${userId};`;

  return await model.query(sQuery, {
    type: QueryTypes.UPDATE,
  }).then(res => {
    return res
  }).catch(err => console.log(err, 'ERROR'))
}

router.post('/', async (req, res, next) => {
  const {
    email,
    name,
    user_id
  } = req.body.userData
  const sldUpdate = await getUserData(user_id)
  if (!sldUpdate.length) {
    await updateSldUpdate(user_id, req.body.fileName)
  } else {
    const isSLDEdited = sldUpdate.some(el => el.sld_update.split(',').some(el => el === req.body.fileName))
    if (isSLDEdited) {
      sldUpdate.forEach(async el => {
        if (el.sld_update.split(',').some(sp => sp === req.body.fileName)) {
          if (el.user_id !== user_id) {
            let tempEntry = el.sld_update.split(',').filter(sp => sp !== req.body.fileName).join(', ')
            if (tempEntry) {
              tempEntry = NULL
            }

            await updateSldUpdate(el.user_id, tempEntry)
            const isUserId = sldUpdate.filter(el => el.user_id === user_id)
            if (isUserId.length) {
              await updateSldUpdate(user_id, isUserId[0].sld_update.concat(', ', req.body.fileName))
            } else {
              await updateSldUpdate(user_id, req.body.fileName)
            }
          }
        }
      })
    } else {
      const isUserId = sldUpdate.filter(el => el.user_id === user_id)
      if (isUserId.length) {
        await updateSldUpdate(user_id, isUserId[0].sld_update.concat(', ', req.body.fileName))
      } else {
        await updateSldUpdate(user_id, req.body.fileName)
      }
    }
  }
  next()
})

module.exports = router