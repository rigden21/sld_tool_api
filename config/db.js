const dotenv = require('dotenv');
dotenv.config()

const config = {
  region: process.env.REGION,
  accessKeyId: process.env.ACCESS_KEY_ID,
  secretAccessKey: process.env.SECRET_ACCESS_KEY,
  sequelize: {
    dialect: process.env.DIALECT,
    database: process.env.DB_NAME,
    // timezone: process.env.TIMEZONE,
    logging: process.env.LOGGING,
    acquire: process.env.ACQUIRE,
    dialectOptions: {
      multipleStatements: process.env.MULTIPLE_STATEMENTS
    },
    pool: {
      max: process.env.POOL,
      min: process.env.POOL
    },
    retry: {
      max: process.env.RETRY
    },
    host: process.env.DB_WRITE_HOST,
    password: process.env.DB_PASSWORD,
    username: process.env.DB_USERNAME
  }
}

module.exports = config