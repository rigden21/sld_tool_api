const express = require('express');
const path = require('path');
const logger = require('morgan');
const multer = require('multer');
const cors = require('cors');

const indexRouter = require('./routes/index');
const svgRouter = require('./routes/svg');
const uploadRouter = require('./routes/upload');
const sldImageCollectionEdit = require('./routes/sldImageCollection')
const jsonRouter = require('./routes/json')
const imoprtS3JsonFile = require('./routes/imoprtS3JsonFile')
const updateJsonUser = require('./routes/updateJsonUser')

const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'temp');
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  // if (!file.originalname.match(/\.(svg|json)$/)) {
  //   return cb(new Error('Please upload a SVG or JSON'))
  // }
  cb(null, true)
};

app.use(multer({ storage: fileStorage, fileFilter: fileFilter, limits: { fieldSize: 8 * 1024 * 1024 } }).single(
  'file'
))
app.use('/temp', express.static(path.join(__dirname, 'temp')));


app.use('/exportJson', updateJsonUser, indexRouter);
app.use('/fileJson', jsonRouter);
app.use('/svg', svgRouter);
app.use('/uploadSVG', sldImageCollectionEdit, uploadRouter);
app.use('/import/s3File', imoprtS3JsonFile);

module.exports = app;
